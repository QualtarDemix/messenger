package org.farrukh.lucanet.messenger.dto;

public class TransactionDTO {
	
	private Integer transactionId;
	
	private Double amount;
	
	private String currency;
	
	private String type;
	
	private Integer parentId;

	public TransactionDTO() {
	}

	public TransactionDTO(Integer transactionId, Double amount, String currency, String type, Integer parentId) {
		this.transactionId = transactionId;
		this.amount = amount;
		this.currency = currency;
		this.type = type;
		this.parentId = parentId;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

}
