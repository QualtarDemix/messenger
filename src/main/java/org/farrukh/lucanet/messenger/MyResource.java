package org.farrukh.lucanet.messenger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.PathParam;
import org.farrukh.lucanet.messenger.dto.TransactionDTO;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import static java.util.stream.Collectors.*;

/**
 * Root resource (exposed at "bookingservice" path)
 */
@Path("bookingservice")
public class MyResource {

    static List<TransactionDTO> transactions = new ArrayList<>();

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }

    @GET
    @Path("/transaction/{transaction_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionDTO getTransaction(@PathParam("transaction_id") Integer transactionId) {
        Optional<TransactionDTO> optionalTransactionDTO = transactions.stream()
                .filter(transaction -> transaction.getTransactionId().equals(transactionId))
                .findFirst();
        return optionalTransactionDTO.orElse(null);
    }

    @GET
    @Path("/types/{type}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransactionDTO> getTypes(@PathParam("type") String type) {
        return transactions.stream()
                .filter(transaction -> transaction.getType().equals(type))
                .collect(toList());
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/transaction/{transaction_id}")
    public String addTransaction(@PathParam("transaction_id") Integer transactionId, TransactionDTO transactionDTORequest) {
        Integer parentId = transactionDTORequest.getParentId();
        if (null != parentId) {
            TransactionDTO existingTransaction = getTransaction(transactionId);
            if (null == existingTransaction) {
                return "{\"status\": \"failed\"}";
            }
        }
        transactionDTORequest.setTransactionId(transactionId);
        transactions.add(transactionDTORequest);
        return "{\"status\": \"ok\"}";
    }

    @GET
    @Path("/currencies")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<String> getCurrencies() {
        return transactions.stream()
                .map(TransactionDTO::getCurrency)
                .collect(toSet());
    }

    @GET
    @Path("/sum/{transaction_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Double> getLinkedTransactions(@PathParam("transaction_id") Integer transactionId) {
        return transactions.stream()
                .filter(transactionDTO -> transactionId.equals(transactionDTO.getParentId()))
                .collect(groupingBy(TransactionDTO::getCurrency,
                        summingDouble(TransactionDTO::getAmount)));
    }
}
